<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function debug($var, $prefix = '') {
	echo "<pre><h3>$prefix</h3>"; var_dump($var); echo '</pre>';
}

function get_bidang() {
	return ['FUKI Corp', 'MuC', 'KMF', 'PAF', 'ITF', 'Syiar', 'Pembinaan', 'Alquran', 'Sospol',  'Humas', 'Media',  'Kontrol Internal', 'Bendahara', 'Controller'];
}

function get_image_bidang() {
	return ['briefcase', 'flower', 'mail', 'heart',  'dev', 'megaphone', 'brightness', 'bookshelf', 'globe',  'profle', 'clapboard', 'gps', 'money', 'settings'];
}

function get_link_wawancara() {
	$res = array(
		'L' => array(
			'Alquran' => 'https://fmardadi.youcanbook.me/',
			'KMF' => 'https://hanifa.youcanbook.me/',
			'PAF' => 'https://eldy.youcanbook.me/',
			'Pembinaan' => 'https://adzkia.youcanbook.me/',
			'MuC' => 'https://youcanbook.me/',
			'Sospol' => 'https://ismail.youcanbook.me/',
			'Syiar' => 'https://nurrochman.youcanbook.me/',
			'Humas' => 'https://farhannp9.youcanbook.me/',
			'Media' => 'https://akhdanbuchou.youcanbook.me/',
			'ITF' => 'https://luthfi.youcanbook.me/',
			'FUKI Corp' => 'https://elang.youcanbook.me/',
		), 
		'P' => array(
			'Alquran' => 'https://fmardadi.youcanbook.me/',
			'KMF' => 'https://hanifa.youcanbook.me/',
			'PAF' => 'https://eldy.youcanbook.me/',
			'Pembinaan' => 'https://adzkia.youcanbook.me/',
			'MuC' => 'https://sarahyarismal.youcanbook.me/',
			'Sospol' => 'https://ismail.youcanbook.me/',
			'Syiar' => 'https://nurrochman.youcanbook.me/',
			'Humas' => 'https://farhannp9.youcanbook.me/',
			'Media' => 'https://akhdanbuchou.youcanbook.me/',
			'ITF' => 'https://luthfi.youcanbook.me/',
			'FUKI Corp' => 'https://elang.youcanbook.me/',
		)
	);
	return $res;
}

function get_number_pil_bidang() {
	return 11;
}

function get_dict_image_bidang() {
	$res = array();
	$bidang = get_bidang();
	$img = get_image_bidang();
	
	for ( $i = 0; $i < count($bidang); $i++ ) {
		$res[$bidang[$i]] = $img[$i];
	}
	
	return $res;
}

function print_error($str, $arr) {
    if ( !empty($arr) ) {
		echo '<div class="alert alert-danger">';
		echo "<h4>$str</h4>";
		echo '<ul>';
		foreach ( $arr as $item ) {
			echo "<li>$item</li>";
		}
		echo '</ul></div>';
	}
}

function print_success($str, $arr) {
    if ( !empty($arr) ) {
		echo '<div class="alert alert-success">';
		echo "<h4>$str</h4>";
		echo '<ul>';
		foreach ( $arr as $item ) {
			echo "<li>$item</li>";
		}
		echo '</ul></div>';
	}
}
