<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

session_start();

class Join extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function index() {
		$this->load->view('front');	
	}
	
	public function logout() {
		$this->session->sess_destroy();
		session_destroy();
		redirect('join/enter');
	}
	
	public function enter() {
		global $data;
		$data = array();
		if ( $this->session->userdata('logged_in') ) {
			if ( $this->session->userdata('id') == -1834 ) {
				redirect('join/admin');
				return;
			}
			
			$data['user'] = $this->registrant->get_info($this->session->userdata('id'))->result()[0];
				
			if ( $this->input->post('upload_1') ) {
				$this->_upload_handler(1);
			}
			else if ( $this->input->post('upload_2') ) {
				$this->_upload_handler(2);
			}
			
			$data['link_tugas'] = $this->registrant->get_assigment_link($data['user']->id)->result()[0];
			
			if ( $this->input->post('change_password') ) {
				$this->_change_password($this->input->post());
			}
			
			$this->load->view('enter', $data);
		}
		else if ( $this->input->post('daftar') ) {	
			$this->_register($this->input->post());
		}
		else if ( $this->input->post('masuk') ) {
			$this->_login($this->input->post());
		}
		else {			
			$this->load->view('form');
		}
	}
	
	public function admin($bidang = 'All', $gender = 'All', $fix = '') {
		if ( $this->_is_admin() ) {
$bidang = urldecode($bidang);
			$data['registrants'] = $this->registrant->get_all($bidang, $gender);
			$data['current_bidang'] = $bidang;
			$data['current_gender'] = $gender;
			$data['fix'] =  $fix;
//debug($data);
			$this->load->view('admin', $data);
		}
		else {
			redirect('join/enter');
		}
	}
	
	public function toggle_status_wawancara($current_bidang, $current_gender, $pil, $id, $val) {
		if ( $this->_is_admin() ) {
			$this->registrant->set_param('status_wawancara_'.$pil, $id, 1-$val);
			redirect('join/admin/'.$current_bidang.'/'.$current_gender);
		}
		else {
			redirect('join/enter');
		}
	}
	
	public function toggle_status_bidang($current_bidang, $current_gender, $pil, $id, $val) {
		if ( $this->_is_admin() ) {
			$this->registrant->set_param('status_bidang_'.$pil, $id, 1-$val);
			
			$user = $this->registrant->get_info($id)->result()[0];
			// Update Bidang Fix
			if ( $user->status_bidang_1 == 1 ) {
				$this->registrant->set_param('bidang_fix', $id, $user->pil_bidang_1);
			}
			else if ( $user->status_bidang_2 == 1 ) {
				$this->registrant->set_param('bidang_fix', $id, $user->pil_bidang_2);
			}
			else {
				$this->registrant->set_param('bidang_fix', $id, "none");
			}
			
			redirect('join/admin/'.$current_bidang.'/'.$current_gender);
		}
		else {
			redirect('join/enter');
		}
	}
	
	private function _is_admin() {
		return $this->session->userdata('logged_in') && $this->session->userdata('id') == -1834;
	}
	
	private function _addFolderToZip($dir, $zipArchive, $zipdir = '') {
	    if (is_dir($dir)) {
	    	// debug($zipdir, 'ZipDir:');
	        if ($dh = opendir($dir)) {
	            //Add the directory
	            if(!empty($zipdir)) {
	            	$zipArchive->addEmptyDir($zipdir);
	            }
	            
	            // Loop through all the files
	            while (($file = readdir($dh)) !== false) {
	                //If it's a folder, run the function again!
	                if(!is_file($dir . $file)){
	          			// debug($dir.$file, 'Directory:');
	                    // Skip parent and root directories
	                    if( ($file !== ".") && ($file !== "..")){
	                    	$nextZipDir = $zipdir === '' ? $file : $zipdir.DIRECTORY_SEPARATOR.$file;
	                        $this->_addFolderToZip($dir . $file . DIRECTORY_SEPARATOR, $zipArchive, $nextZipDir);
	                    }
	                  
	                }else{
	                    // Add the files
	            		$nextZipDir = $zipdir === '' ? $file : $zipdir.DIRECTORY_SEPARATOR.$file;
	                    $zipArchive->addFile($dir . $file, $nextZipDir);
	                  
	                }
	            }
	            
	            closedir($dh);
	        }
	    }
	}
	
	public function download_tugas($bidang) {
		if ( $this->_is_admin() ) {
		    $zip = new ZipArchive;
		    // $dir = APPPATH.'../uploads/'.$bidang;
		    // $zip_file = $dir.'/../'.$bidang.'.zip';
		    $dir = realpath(APPPATH.'../uploads/').DIRECTORY_SEPARATOR.$bidang.DIRECTORY_SEPARATOR;
		    $zip_file = realpath(APPPATH.'../uploads/').DIRECTORY_SEPARATOR.$bidang.'.zip';
		    // debug($dir);
		    // debug($zip_file);
		    
		    if (true !== $zip->open($zip_file, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE)) {
		        return false;
		    }
		    
		    $this->_addFolderToZip($dir, $zip);
		    
		    $zip->close();
		    
		    header('Content-Type: application/zip');
		    header("Content-Disposition: attachment; filename='".$bidang.".zip'");
		    header('Content-Length: ' . filesize($zipname));
		    header("Location: ".base_url().'uploads/'.$bidang.".zip");

		    return $zip;
		}
		else {
			redirect('join/enter');
		}
	}
	
	private function _upload_handler($pil) {
		global $data;
		$error = array();
		$success = array();
		
		$bidang = ($pil == 1 ? $data['user']->pil_bidang_1 : $data['user']->pil_bidang_2);
		
		$config = array(
			'file_name' => $bidang.'-'.$data['user']->nama.'-'.$data['user']->npm.'.zip',
			'upload_path' => APPPATH.'../uploads/'.($pil == 1  ? $data['user']->pil_bidang_1 : $data['user']->pil_bidang_2).'/'.$pil,
			'allowed_types' => 'zip',
			'max_size' => '2000000',
			'overwrite' => TRUE
		);
		
		$this->load->library('upload', $config);
		
		if ( !$this->upload->do_upload('file_tugas_'.$pil) ) {
			$error[] = $this->upload->display_errors();
		}
		else {
			// $success[] = $this->upload->data();
			$success[] = 'File uploaded successfully.';
			$file_url = base_url()."uploads/$bidang/$pil/".$this->upload->data()['file_name'];
			$this->registrant->change_assigment_link($data['user']->id, $pil, $file_url);
		}
		//debug($file_url);
		$data['upload_error'] = $error;
		$data['upload_success'] = $success;
	}
	
	private function _register($params) {
		global $data;
		$error = array();
		$success = array();
		
		// user submit register form			
		$row = array(
			'nama' => htmlspecialchars(trim($params['nama'])),
			'npm' => htmlspecialchars(trim(trim($params['npm']))),
			'jurusan' => htmlspecialchars(trim(trim($params['jurusan']))),
			'email' => htmlspecialchars(trim($params['email'])),
			'gender' => htmlspecialchars(trim($params['gender'])),
			'no_hp' => htmlspecialchars(trim($params['no_hp'])),
			'pil_bidang_1' => htmlspecialchars(trim($params['pil_bidang_1'])),
			'pil_bidang_2' => htmlspecialchars(trim($params['pil_bidang_2'])),
			'joined_date' => date("Y-m-d H:i:s"),
		);
		
		extract($row);
		
		$data['post'] = $row;
		if ( $this->_validate_form($row) == TRUE || $email === 'alpancs@gmail.com') {
			// create password
			$salt = openssl_random_pseudo_bytes(10);
			$salt = bin2hex($salt);
			
			$pass = openssl_random_pseudo_bytes(5);
			$pass = bin2hex($pass);
			
			$hash = hash('sha256', $pass . $salt);
			
			$row['salt'] = $salt;
			$row['password'] = $hash;
			
			// valid, write data to the database
			$this->registrant->insert($row);
			
			// email password
			$row['password'] = $pass;
			$data['post'] = $row;
			$this->_sendmail($row);
			
			// show success message
			$success[] = 'Terimakasih. Registrasi Berhasil. Email berisi password telah dikirimkan ke '.$email.'. Silahkan menggunakan alamat email serta password tersebut untuk login. Diharapkan untuk mengubah password setelah berhasil login.';
		}
		else {
			// show error message
			$error[] = 'Maaf, Anda sudah terdaftar di database kami. Kontak kami jika ada masalah.';
		}
		
		$data['register_error'] = $error;
		$data['register_success'] = $success;
		$this->load->view('form', $data);
	}
	
	private function _sendmail($info)
	{
		$info['kode'] = "ganteng";
	    $url = 	"http://alfan.coderhutan.com/bejometer/numpang/oprecfuki2014?".http_build_query($info);
	    $debug = file_get_contents($url);
	}
	
	private function _login($params) {
		global $data;
		$error = array();
		$success = array();
		
		// user submit login form
		$row = array(
			'email' => htmlspecialchars(trim($params['email'])),
			'password' => htmlspecialchars(trim($params['password']))
		);
		
		extract($row);
		
		if ( $email == 'admin@admin' && $password == 'cokelat' ) {
			$this->session->set_userdata('id', -1834);
			$this->session->set_userdata('logged_in', true);
			redirect('join/admin');
			return;
		}
		
		$res = $this->registrant->get_login($email);
		
		if ( $res->num_rows() > 0 ) {
			$res = $res->result()[0];
			$salt = $res->salt;
			$hash = $res->password;
			
			if ( hash('sha256', $password.$salt) == $hash ) {
				// login success 
				// set session
				$this->session->set_userdata('id', $res->id);
				$this->session->set_userdata('logged_in', true);
				redirect('join/enter');
				return;
			}
			else {
				// login fail, invalid password
				$error[] = 'Email atau password salah.';
			}
		}
		else {
			// login fail, invalid email
			$error[] = 'Email atau password salah.';
		}
		
		$data['login_error'] = $error;
		$data['login_success'] = $success;
		$this->load->view('form', $data);
	}
	
	private function _change_password($params) {
		global $data;
		$error = array();
		$success = array();
		
		$row = array(
			'old_password' => htmlspecialchars(trim($params['old_password'])),
			'new_password' => htmlspecialchars(trim($params['new_password'])),
			'confirm_new_password' => htmlspecialchars(trim($params['confirm_new_password']))
		);
		
		extract($row);
		
		$res = $this->registrant->get_password($this->session->userdata('id'));
		
		$res = $res->result()[0];
		$salt = $res->salt;
		$hash = $res->password;
			
		if ( hash('sha256', $old_password.$salt) != $hash ) $error[] = 'Password lama salah';			
		if ( $new_password != $confirm_new_password ) $error[] = 'Password baru tidak cocok.';
		if ( strlen($new_password) < 5 ) $error[] = 'Password baru minimal 5 karakter.';
		
		if ( empty($error) ) {
			$salt = openssl_random_pseudo_bytes(10);
			$salt = bin2hex($salt);
			
			$hash = hash('sha256', $new_password. $salt);
			
			$this->registrant->change_password($this->session->userdata('id'), $salt, $hash);
			$success[] = 'Password berhasil diubah.';
		}
		
		$data['change_password_error'] = $error;
		$data['change_password_success'] = $success;
	}
	
	private function _validate_form($row) {
		return !$this->registrant->is_exist($row);
	}

	public function lupapassword($email)
	{
		if ( $this->_is_admin() ) {
			$email = rawurldecode($email);
			if ($this->registrant->is_exist(array("npm"=>"1206219086","email"=>$email))) {
				// create password
				$salt = openssl_random_pseudo_bytes(10);
				$salt = bin2hex($salt);
				
				$pass = openssl_random_pseudo_bytes(5);
				$pass = bin2hex($pass);
				
				$hash = hash('sha256', $pass . $salt);
				
				$kode = "ganteng";
				$url = 	"http://alfan.coderhutan.com/bejometer/numpang/oprecfuki2014/emailreset/".
		    		rawurlencode($email)."/".
		    		rawurlencode($pass)."/".
		    		rawurlencode($kode);
		    	$debug = file_get_contents($url);

				$this->registrant->change_password2($email, $salt, $hash);
				echo "Password kamu sudah di-reset. Password baru akan kami kirim ke $email. Cek email ya. :)";
			}
			else
			{
				echo "Maaf, alamat $email belum digunakan untuk mendaftar.";
			}
		}
		else {
			redirect('join/enter');
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */