<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrant extends CI_Model {
	function is_exist($params) {
		return $this->db->get_where('fuki17_registrant', array('npm' => $params['npm']), 1)->num_rows() > 0 || $this->db->get_where('fuki17_registrant', array('email' => $params['email']), 1)->num_rows() > 0;
	}
	
	
	function get_all($bidang, $gender) {
		if($bidang !== 'All') $this->db->where("(pil_bidang_1='$bidang' OR pil_bidang_2='$bidang')");
		if($gender !== 'All') $this->db->where('gender',$gender);
		return $this->db->get('fuki17_registrant');
	}
	
	function get_login($email) {
		$this->db->select('id,password,salt');
		return $this->db->get_where('fuki17_registrant', array('email' => $email), 1);
	}
	
	function get_info($id) {
		// $this->db->select('id,nama,npm,email,joined_date,pil_bidang_1,pil_bidang_2');
		return $this->db->get_where('fuki17_registrant', array('id' => $id), 1);
	}
	
	function insert($row) {
		$this->db->insert('fuki17_registrant', $row);
	}
	
	function get_password($id) {
		$this->db->select('password,salt');
		return $this->db->get_where('fuki17_registrant', array('id' => $id), 1);
	}
	
	function change_password($id, $salt, $password) {
		$this->db->where('id', $id);
		$this->db->update('fuki17_registrant', array('salt' => $salt, 'password' => $password));
	}

	function change_password2($email, $salt, $password) {
		$this->db->where('email', $email);
		$this->db->update('fuki17_registrant', array('salt' => $salt, 'password' => $password));
	}
	
	function get_assigment_link($id) {
		$this->db->select('link_tugas_1,link_tugas_2');
		return $this->db->get_where('fuki17_registrant', array('id' => $id), 1);
	}
	
	function change_assigment_link($id, $pil, $link) {
		$this->db->where('id', $id);
		$this->db->update('fuki17_registrant', array('link_tugas_'.$pil => $link));
	}
	
	function set_status_wawancara($pil, $id, $val) {
		$this->db->where('id', $id);
		$this->db->update('fuki17_registrant', array('status_wawancara_'.$pil => $val));
	}
	
	function set_param($param, $id, $val) {
		$this->db->where('id', $id);
		$this->db->update('fuki17_registrant', array($param => $val));
	}
}