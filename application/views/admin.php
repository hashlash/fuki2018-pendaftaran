<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>FUKI 2015</title>
        <meta name="viewport" content="width=device-width">
        
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.ico" type="image/x-icon">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fixed_table_rc.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
        <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <section id="wrapper">
        <a href="<?php echo base_url();?>index.php/join/logout">Logout</a>
        <h1>Admin</h1>
        <div>
            <h3>Filter</h3>
            <h5>Bidang</h5>
            <?php
                // debug($current_gender);
                // debug($current_bidang);
                if ( !isset($current_gender) ) $current_gender = 'All';
                if ( !isset($current_bidang) ) $current_bidang = 'All';
            ?>
            <div class="btn-group">
            <?php
                 $list_bidang = get_bidang();
                 array_splice($list_bidang, 0, 0, "All");
                 for ( $i = 0; $i < get_number_pil_bidang()+1; $i++ ) {
                     echo '<a href="'.base_url().'index.php/join/admin/'.$list_bidang[$i].'/'.$current_gender.'" class="btn btn-default'.($list_bidang[$i] == $current_bidang ? ' active' : '').'">'.$list_bidang[$i].'</a>';
                 }
            ?>
            </div>
            <h5>Gender</h5>
            <div class="btn-group">
                <a href="<?php echo base_url().'index.php/join/admin/'.$current_bidang.'/All';?>" class="btn btn-default<?php echo ($current_gender == 'All' ? ' active' : '');?>">All</a>
                <a href="<?php echo base_url().'index.php/join/admin/'.$current_bidang.'/L';?>" class="btn btn-default<?php echo ($current_gender == 'L' ? ' active' : '');?>">L</a>
                <a href="<?php echo base_url().'index.php/join/admin/'.$current_bidang.'/P';?>" class="btn btn-default<?php echo ($current_gender == 'P' ? ' active' : '');?>">P</a>
            </div>
        </div>
<h5>Fix</h5>
<div class="btn-group">
<a href="<?php echo base_url().'index.php/join/admin/'.$current_bidang.'/All/fix';?>" class="btn btn-default">Fix</a>
<a href="<?php echo base_url().'index.php/join/admin/'.$current_bidang.'/All/none';?>" class="btn btn-default">Galau</a>
</div>
        <h3>Pendaftar</h3>
        <div class="table-wrapper">
        <table id="registrant" class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>L/P</th>
                    <th>NPM</th>
                    <th>Prodi</th>
                    <th>Pil Bidang 1</th>
                    <th>ST1</th>
                    <th>SW1</th>
                    <th>Pil Bidang 2</th>
                    <th>ST2</th>
                    <th>SW2</th>
                    <th>Bidang Fix</th>
                    <th>Email</th>
                    <th>No HP</th>
                    <th>Time remain</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $counter = 1;
            foreach ( $registrants->result() as $registrant ) {
                extract(get_object_vars($registrant));
                
                if ( isset($fix) && $fix == 'fix' && $bidang_fix != $current_bidang ) continue;
                if ( isset($fix) && $fix == 'none' && $bidang_fix != 'none' ) continue;
                
                ?><tr><?php
                
                echo "<td>$counter</td>";
                echo "<td>$nama</td>";
                echo "<td>$gender</td>";
                echo "<td>$npm</td>";
                echo "<td>".strtoupper($jurusan)."</td>";
                
                $sw1 = ($status_wawancara_1 == 0 ? 'no' : 'yes');
                $sw2 = ($status_wawancara_2 == 0 ? 'no' : 'yes');
                
                $st1 = $link_tugas_1 == '404' ? 'no' : 'yes';
                $st2 = $link_tugas_2 == '404' ? 'no' : 'yes';
                
                echo 
                "<td class='".($status_bidang_1 == 1 ? 'blue_bg' : 'maybe')."'>
                <a 
                    href='".base_url().'index.php/join/toggle_status_bidang/'.$current_bidang.'/'.$current_gender.'/1/'.$id.'/'.$status_bidang_1."'>"
                    .($status_bidang_1 == 1 ? '<span class="glyphicon glyphicon-heart"></span> ' : '').
                "$pil_bidang_1
                </a>
                </td>";
                echo 
                "<td class='$st1'>".($link_tugas_1 == '404' ? '<span class=" glyphicon glyphicon-ban-circle"></span>' : "<a href='$link_tugas_1'><span class='glyphicon glyphicon-cloud-download'></span></a>").
                "</td>";
                echo "<td class='$sw1'><a href='".base_url().'index.php/join/toggle_status_wawancara/'.$current_bidang.'/'.$current_gender.'/1/'.$id.'/'.$status_wawancara_1."'>".($sw1 == 'yes' ? '<span class="glyphicon glyphicon-ok"></span>' : '<span class="glyphicon glyphicon-remove"></span>')."</a></td>";
                
                echo 
                "<td class='".($status_bidang_2 == 1 ? 'blue_bg' : 'maybe')."'>
                <a 
                    href='".base_url().'index.php/join/toggle_status_bidang/'.$current_bidang.'/'.$current_gender.'/2/'.$id.'/'.$status_bidang_2."'>"
                    .($status_bidang_2 == 1 ? '<span class="glyphicon glyphicon-heart"></span> ' : '').
                "$pil_bidang_2
                </a>
                </td>";
                echo 
                "<td class='$st2'>".($link_tugas_2 == '404' ? '<span class=" glyphicon glyphicon-ban-circle"></span>' : "<a href='$link_tugas_2'><span class='glyphicon glyphicon-cloud-download'></span></a>").
                "</td>";
                echo "<td class='$sw2'><a href='".base_url().'index.php/join/toggle_status_wawancara/'.$current_bidang.'/'.$current_gender.'/2/'.$id.'/'.$status_wawancara_2."'>".($sw2 == 'yes' ? '<span class="glyphicon glyphicon-ok"></span>' : '<span class="glyphicon glyphicon-remove"></span>')."</a></td>";
                
                echo "<td class='".($bidang_fix == 'none' ? '' : 'blue_bg')."'>$bidang_fix</td>";
                echo "<td>$email</td>";
                echo "<td>$no_hp</td>";
                
            $time_remain = min(86400*5+DateTime::createFromFormat('Y-m-d H:i:s', $joined_date)->format('U'), 1487116799)-time();
 
                $tmp = $time_remain;
            
                if ( $tmp > 0 ) {
                    $day_remain = floor($time_remain/86400);
                    $time_remain %= 86400;
                    $hour_remain = floor($time_remain/3600);
                    $time_remain %= 3600;
                    $minute_remain = floor($time_remain/60);
                    
                    echo "";
                    echo "<td>{$day_remain}d {$hour_remain}h {$minute_remain}m</td>";
                }
                else {
                    echo '<td class="no">Time Out!</td>';
                }
                ?></tr><?php
                
                $counter++;
            }
            ?>
            </tbody>
        </table>
        </div>
        <div class="alert alert-info">
            <h4>Short Guide:</h4>
            <ul>
            <li><strong>Filter digunakan untuk menyaring perbidang/pergender</strong></li>- Kalo dipilih selain All bakal menampilkan pendaftar yang sesuai dengan kriteria filter
            <li><strong>Pil Bidang 1/Pil Bidang 2 (data di tabel bisa diklik)</strong></li>
            - Kuning artinya belum diterima, kalo diklik jadi biru ada hatinya <span class="glyphicon glyphicon-heart"></span> artinya diterima di bidang tersebut. Kalo pas biru di klik lagi balik kuning.<br>
            <li><strong>ST1/ST2 = Status Tugas 1/2 (data di tabel bisa diklik)</strong></li>
            - <span class="glyphicon glyphicon-cloud-download"></span> artinya dah upload, jika diklik bakal download itu file punya orang, <span class="glyphicon glyphicon-ban-circle"></span> belum upload<br>
            - Kalau mau download seluruh tugas per bidang bisa pake link yang di bawah<br>
            - Update otomatis<br>
            <li><strong>SW1/SW2 = Status Wawancara 1/2 (data di tabel bisa diklik)</strong></li>
            - <span class="glyphicon glyphicon-ok"></span> artinya sudah selesai wawancara (face-to-face), <span class="glyphicon glyphicon-remove"></span> belum<br>
            - Klik pada icon untuk mengganti dari <span class="glyphicon glyphicon-ok"></span> ke <span class="glyphicon glyphicon-remove"></span> dan sebaliknya (toggle).<br>
            - <strong>Ini updatenya manual</strong><br>
            <li><strong>Bidang Fix</strong></li>
            - Update otomatis<br>
            - Bidang fix yang dimasuki pendaftar. Kondisinya:<br>
<pre>if ( Pil Bidang 1 diterima ) { Bidang Fix = Pil Bidang 1 }
else {
    if ( Pil Bidang 2 diterima ) { Bidang Fix = Pil Bidang 2 }
    else {
        Bidang Fix = 'none' -> artinya ga diterima dimana-mana
    }
}</pre>
            </ul>
        </div>
        <h3>Link Download Tugas Per Bidang</h3>
        <?php
             $list_bidang = get_bidang();
             for ( $i = 0; $i < get_number_pil_bidang(); $i++ ) {
                 echo '<a href="'.base_url().'index.php/join/download_tugas/'.$list_bidang[$i].'" class="btn btn-default">'.$list_bidang[$i].'</a> ';
             }
        ?>
        </section>
        
        <br>
        
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.9.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/fixed_table_rc.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        <script>
            $(document).ready(function() {
                // return;
                $('#registrant').fxdHdrCol({
                    fixedCols: 2,
                    width: "100%",
                    height: 400,
                    colModal: [
                        {width: 30, align: 'center'},    // No
                        {width: 200, align: 'center'},   // Nama
                        {width: 30, align: 'center'},    // L/P
                        {width: 100, align: 'center'},    // NPM
                        {width: 50, align: 'center'},    //Prodi
                        {width: 80, align: 'center'},    // Pil Bid 1
                        {width: 40, align: 'center'},    // ST1
                        {width: 40, align: 'center'},    // SW1
                        {width: 80, align: 'center'},    // Pil Bid 2
                        {width: 40, align: 'center'},    // ST2
                        {width: 40, align: 'center'},    // SW2
                        {width: 80, align: 'center'},    // Bidang Fix
                        {width: 200, align: 'center'},    // Email
                        {width: 110, align: 'center'},    // No HP
                        {width: 110, align: 'center'}    // date joined
                    ]
                });
            })
        </script>
    </body>
</html>
