<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>FUKI 2016</title>
        <meta name="viewport" content="width=device-width">
        
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.ico" type="image/x-icon">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
        <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
        
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,300,200&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    
        <?php
        $image_bidang = get_image_bidang();
        $bidang = get_bidang();
        ?>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <div style="background: #4F5D73; height: 10px; width: 100%"></div>
        <div id="wrapper">
        
        <div class="row">
            <a href="<?php echo base_url();?>" style="color: #333333; text-decoration: none">
            <div class="col-lg-1 col-md-1 col-sm-1" style="padding-right: 0">
                <img src="<?php echo base_url().'assets/img/logo fuki black.png';?>" style="margin-top: 27px">
            </div>
            
            <div class="col-lg-11 col-md-11 col-sm-11" style="padding-left: 0">
            <h1 style="margin-bottom: 2px;">OPREC FUKI 2017</h1>
            <h3 style="margin-top: 0">#TerbanglahBersamaku</h3>
            </div>
            </a>
        </div>
        <br>
        
        <div class="row">
        
        <div class="col-lg-4 col-md-4"></div>
        
        <div class="col-lg-4 col-md-4">
        <h2>Masuk</h2>
        <?php if ( isset($login_error) ) print_error('Login gagal', $login_error);?>
        <small>Sudah daftar? Silahkan masuk</small>    
        <form method="post">
            <div class="form-group">
            <h4>Email</h4><input type="email" class="form-control" name="email" required placeholder="Masukkan email"></label>
            </div>
            <div class="form-group">
            <h4>Password</h4><input type="password" class="form-control" name="password" required placeholder="Masukkan Password">
            </div><br>
            <input type="submit" class="btn btn-info btn-lg" name="masuk" value="Masuk" style="width:100%;">
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
        </form>
        </div> <!-- / .col -->
        <div class="col-lg-4 col-md-4"></div>
        </div> <!-- / .row -->
        </div> <!-- / #wrapper -->
        <div style="background: #4F5D73; height: 15px; width: 100%"></div>
        
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.9.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        <script>
            $(document).ready(function() {   
                $('.fc').hide().removeClass('fuki-hide');
                function setting_img(id) {  
                    $('#'+id+' .box-bidang').on('click', function() {
                        if ( $(this).hasClass('selected') ) {
                            // turn off
                            $(this).find('img.fc').hide();
                            $(this).find('img.bw').show();
                            
                            $(this).removeClass('selected');
                            $('#'+id+' input').val('');
                        }
                        else {
                            // turn on
                            $('#'+id+' .box-bidang.selected').removeClass('selected');
                            $('#'+id+' .box-bidang .fc').hide();
                            $('#'+id+' .box-bidang .bw').show();
                            
                            $(this).find('img.bw').hide();
                            $(this).find('img.fc').show();
                            
                            $(this).addClass('selected');
                            $('#'+id+' input').val($(this).find('h4').html());
                            $('#'+id+' .awas').html(''); // empty alert
                        }
                    });
                }
                setting_img('pil_bidang_1');
                setting_img('pil_bidang_2');
                $('#daftar').on('click', function(e) {
                   if ( $('#pil_bidang_1 input').val() === "" ) {
                       e.preventDefault();
                       $('#pil_bidang_1 .awas').html('<div class="alert alert-danger">Pilihan bidang 1 harus dipilih.</div>');
                   } 
                   
                   if ( $('#pil_bidang_2 input').val() == $('#pil_bidang_1 input').val() ) 
                       $('#pil_bidang_2 input').val('none');
                });
            });
        </script>
    </body>
</html>
