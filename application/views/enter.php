<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>FUKI 2017</title>
        <meta name="viewport" content="width=device-width">
        
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.ico" type="image/x-icon">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css"><link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
        <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
        
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,300,200&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <div style="background: #4F5D73; height: 10px; width: 100%"></div>
        <div id="wrapper">
        <div class="row">
            <a href="<?php echo base_url();?>" style="color: #333333; text-decoration: none">
            <div class="col-lg-1 col-md-1 col-sm-1" style="padding-right: 0">
                <img src="<?php echo base_url().'assets/img/logo fuki black.png';?>" style="margin-top: 27px">
            </div>
            
            <div class="col-lg-11 col-md-11 col-sm-11" style="padding-left: 0">
            <h1 style="margin-bottom: 2px;">PEREKRUTAN TERBUKA FUKI 2017</h1>
            <h3 style="margin-top: 0">#MengangkasaBersama</h3>
            </div>
            </a>
        </div>
        
        <?php 
        // debug($user); 
        ?>
        
        <div class="row">
            <div class="col-lg-7">
        
            <br>
            <h3>Selamat datang, <?php echo $user->nama; ?>
            <a class="btn btn-danger"  href="<?php echo base_url();?>index.php/join/logout"> Logout</a></h3>
            <br>
            
            <?php
            if ( isset($upload_error) ) 
                print_error('Tugas gagal diupload.', $upload_error);
            if ( isset($upload_success) ) 
                print_success('Tugas berhasil diupload.', $upload_success);
            ?>
            <!-- Download Tugas -->
            <h2>Download Tugas</h2>
            <div class="btn-group">
<a class="btn btn-default" href="<?php echo base_url().'uploads/Tugas/Tugas Umum 2017.docx';?>">Tugas Umum</a>
            <a class="btn btn-default" target="_blank" href="<?php echo base_url().'uploads/Tugas/'.$user->pil_bidang_1.' 2017.docx';?>"><?php echo $user->pil_bidang_1;?></a>
            <?php if ( $user->pil_bidang_2 != 'none' ) { ?>
            <a class="btn btn-default" target="_blank"  href="<?php echo base_url().'uploads/Tugas/'.$user->pil_bidang_2.' 2017.docx';?>"><?php echo $user->pil_bidang_2;?></a>
            <?php } ?>
            </div>
            <br><br>
            
            <!-- Slot wawancara -->
            <h2>Link slot wawancara</h2>
<small>*link slot wawancara akan diupdate saat jadwal kuliah sudah fix</small><br>
<small>Anda diwajibkan mengisi slot wawancara di masing-masing bidang.</small><br><br>
            <div class="btn-group">
            <?php
            $link_wawancara = get_link_wawancara();
            ?>
            <a class="btn btn-default" target="_blank" href="<?php echo $link_wawancara[$user->gender][$user->pil_bidang_1]; ?>"><?php echo $user->pil_bidang_1;?></a>
            <?php if ( $user->pil_bidang_2 != 'none' ) { ?>
            <a class="btn btn-default" target="_blank" href="<?php echo $link_wawancara[$user->gender][$user->pil_bidang_2];?>"><?php echo $user->pil_bidang_2;?></a>
            <?php } ?>
            </div>
            <br><br>
                
            <!-- Upload Tugas -->
            <h2>Upload Tugas</h2>
            <?php 
            $time_remain = min(86400*5+DateTime::createFromFormat('Y-m-d H:i:s', $user->joined_date)->format('U'), 1487116799)-time();
            $tmp = $time_remain;
            
            $day_remain = floor($time_remain/86400);
            $time_remain %= 86400;
            $hour_remain = floor($time_remain/3600);
            $time_remain %= 3600;
            $minute_remain = floor($time_remain/60);
            
            if ( $tmp > 0 ) { ?>
                <!-- Masih bisa upload -->
            <h4 class="alert alert-warning">Sisa waktu: <strong><?php echo "$day_remain hari $hour_remain jam $minute_remain menit";?></strong></h4>
            
                <!-- Bidang 1 -->
            <h3>Bidang <?php echo $user->pil_bidang_1;?></h3>
            <?php if ( $link_tugas->link_tugas_1 != '404' ) {?>
            <a href="<?php echo $link_tugas->link_tugas_1;?>"><?php echo basename($link_tugas->link_tugas_1);?></a><br>
            <?php } ?>
            <p>Tugas umum dan bidang digabung dalam 1 folder kemudian dikompres dengan format zip. File akan di-rename otomatis oleh sistem.</p>
            <form enctype="multipart/form-data" method="post">
                <input type="hidden" name="MAX_FILE_SIZE" value="2000000">
                <div class="row">
                    <div class="col-lg-9"><input type="file" name="file_tugas_1" required></div>
                    <br/>
                    <div class="col-lg-3"><input type="submit" class="btn btn-info" name="upload_1" value="Upload Tugas"></div>
                </div>
                (Max: 2MB, format .zip)
            </form>
            
                <!-- Bidang 2, jika ada -->
            <?php if ( $user->pil_bidang_2 != 'none' ) { ?>
            <br>
            <div style="background: #4F5D73; height: 5px; width: 100%"></div>
            <h3>Bidang <?php echo $user->pil_bidang_2;?></h3>
            <?php if ( $link_tugas->link_tugas_2 != '404' ) {?>
            <a href="<?php echo $link_tugas->link_tugas_2;?>"><?php echo basename($link_tugas->link_tugas_2);?></a><br>
            <?php } ?>
            <p>Tugas umum dan bidang digabung dalam 1 folder kemudian dikompres dengan format zip. File akan di-rename otomatis oleh sistem.</p>
            <form enctype="multipart/form-data" method="post">
                <input type="hidden" name="MAX_FILE_SIZE" value="2000000">
                <div class="row">
                    <div class="col-lg-9"><input type="file" name="file_tugas_2" required></div>
                    <br/>
                    <div class="col-lg-3"><input type="submit" class="btn btn-info" name="upload_2" value="Upload Tugas"></div>
                </div>
                (Max: 2MB, format .zip)
            </form>
            <?php } ?>
            <?php } else { ?>
                <!-- Gabisa Upload lagi -->
            <h4 class="alert alert-danger">Slot upload tugas telah ditutup.</h4>
            <?php } ?>
            <br><br>
            <small>Silahkan hubungi +62 8561790785 (Ayu Fatmawati) atau Kadept masing-masing bidang jika ada masalah.</small>
            
            <br><br><br><br>
            </div>
            
            <div class="col-lg-1"></div>
            
            <div class="col-lg-4">
            <!-- Ubah password -->
            <h2>Ubah Password</h2>
            <?php
            if ( isset($change_password_error) ) 
                print_error('Password gagal diubah.', $change_password_error);
            if ( isset($change_password_success) ) 
                print_success('Password berhasil diubah.', $change_password_success);
            ?>
            <form method="post">
                <div class="form-group">
                <h4>Old Password</h4><input type="password" class="form-control" name="old_password" required placeholder="Password Lama">
                </div>
                <div class="form-group">
                <h4>New Password</h4><input type="password" class="form-control" name="new_password" required placeholder="Password Baru">
                </div>
                <div class="form-group">
                <h4>Confirm Password</h4><input type="password" class="form-control" name="confirm_new_password" required  placeholder="Ulang Password Baru">
                </div>
                <input type="submit" class="btn btn-info btn-lg" style="width: 100%" name="change_password" value="Ubah Password">
            </form>
            </div>
        </div> <!-- /row -->
        
        
        </div> <!-- /wrapper -->
        <div style="background: #4F5D73; height: 15px; width: 100%"></div>
        
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.9.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
    </body>
</html>
