<!doctype html>
<html>
<head lang="en">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
    <title>FUKI 2017</title>
	<meta content="Open Recruitment FUKI 2017" name="description">
<meta content="fasilkom, fuki, oprec, recruitment, 2017," name="keywords">
<meta content="" property="og:title">

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.ico" type="image/x-icon">
    
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/';?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url().'assets/front/';?>css/font-awesome.min.css" rel="stylesheet">
     
    <link rel="stylesheet" href="<?php echo base_url().'assets/front/';?>css/component.css">
    	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/';?>css/default.css">


	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/front/';?>css/style.css">	
	
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,300,200&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
          <script src="<?php echo base_url().'assets/front/';?>js/modernizr.custom.js"></script>
</head>

<body>
	<?php 
	$img = get_dict_image_bidang(); 
	// debug($img); 
	?>
	<div class="navbar navbar-fixed-top" data-activeslide="1">
		
		
			<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
			
			<div class="nav-collapse collapse navbar-responsive-collapse">
				<ul class="nav row">
					<li data-slide="1" class="col-sm-2 menu-item"><a id="menu-link-1" href="#slide-1" title="Next Section"><span class="icon icon-home"></span> <span class="text"> FUKI 2017</span></a></li>					
<!--
					<li data-slide="tambahan" class="col-sm-2 menu-item"><a id="menu-link-1-2" href="#tambahan" title="Next Section"><span class="icon icon-user"></span> <span class="text"> DEWAN PERTIMBANGAN</span></a></li>
	-->				
					
					
					<li data-slide="2" class="col-sm-2 menu-item"><a id="menu-link-2" href="#slide-2" title="Next Section"><span class="icon icon-user"></span> <span class="text"> KI & MUC</span></a></li>
					<li data-slide="3" class="col-sm-2 menu-item"><a id="menu-link-3" href="#slide-3" title="Next Section"><span class="icon icon-user"></span> <span class="text"> KEUANGAN</span></a></li>
					<li data-slide="4" class="col-sm-2 menu-item"><a id="menu-link-4" href="#slide-4" title="Next Section"><span class="icon icon-heart"></span> <span class="text"> INTERNAL</span></a></li>
					<li data-slide="5" class="col-sm-2 menu-item"><a id="menu-link-5" href="#slide-5" title="Next Section"><span class="icon icon-globe"></span> <span class="text"> S Y I A R</span></a></li>
					<li data-slide="6" class="col-sm-2 menu-item"><a id="menu-link-6" href="#slide-6" title="Next Section"><span class="icon icon-gears"></span> <span class="text"> RE LA SI</span></a></li>

					<li style="display: none;" data-slide="7" class="col-sm-2 menu-item"><a id="menu-link-7" href="#slide-7" title="Next Section"><span class="icon icon-envelope"></span> <span class="text"> KONTAK</span></a></li>


				</ul>

				<div class="row">
					<div class="col-sm-2 active-menu"></div>
				</div>
			</div><!-- /.nav-collapse -->
		</div><!-- /.container -->
	</div><!-- /.navbar -->
	
	
	<!-- === Arrows === -->
	<div id="arrows">
		<div id="arrow-up" class="disabled"></div>
		<div id="arrow-down"></div>
		<div id="arrow-left" class="disabled visible-lg"></div>
		<div id="arrow-right" class="disabled visible-lg"></div>
	</div><!-- /.arrows -->
	
<div class="md-modal md-effect-10" id="modal-syiar">
			<div class="md-content">
				<h3>Syiar</h3>
				<div>
					<p>Tugas syiar ngapain kak? berat yah :( ?
<br>Wahhh, jangan bilang gitu dong. berdakwah itu gak seberat itu kok, bisa dilakukan dengan hal mudah. Kan senyum itu juga salah satu metode dakwah. Tersenyum gak sulit kan :) Menyediakan ta&#39;jil bagi yang berpuasa juga mudah loh.
<br>Oiya kamu tahu gak Allah berfirman dalam Al-Qur&#39;an yang berbunyi &#34;Dan hendaklah ada di antara kalian sekelompok orang yang menyeru kepada kebajikan, menyuruh kepada yang ma&#39;ruf dan mencegah dari yang munkar. Mereka itulah orang&#45;orang yang beruntung,&#34; (QS Ali &#39;Imran [3], 104)
<br>Masa iya sih gak mau jadi orang yang beruntung?
<br>Jadi tunggu apa lagi?? Kuylah #JoinSyiarRameRame
<br>Jadilah bagian dari Keluarga FUKI 2017!

					</p>
					<!-- <br><a href="http://chirpstory.com/li/247304" class="btn btn-link" role="button">Link chirpstory</a> -->
					<button class="md-close"><strong>Close!</strong></button>
				</div>
			</div>
		</div>
<div class="md-modal md-effect-10" id="modal-pembinaan">
			<div class="md-content">
				<h3>Pembinaan</h3>
				<div>
					<p>Pada berbagai rentang masa dalam berbagai bidang kehidupan, Pembinaan sudah ada lho! Keren kan?!
Sejak jaman Rasulullah, Rasul sudah melakukan pembinaan dengan menyampaikan ajaran Islam pada sahabat-sahabatnya, begitu juga dengan Sultan Muhammad Al-Fatih, sang penakluk muda yang di usia 21 dapat mewujudkan cita-cita Rasulullah SAW untuk menaklukkan Konstantinopel, menyeleksi dengan mengontrol ibadah dan ruhiyah pasukan perangnya.<br>
Waahh kok berat? Saya kan gak sehebat Rasulullah atau sekeren Muhammad Al-Fatih. Tenang tenang.. Justru di sinilah kalian bisa mengakselerasi diri, berkembang agar lebih baik lagi!
Kalau FUKI ibarat rumah, Departemen ini ibarat dapurnya! kita akan memberi sumber energi untuk warga Fasilkom dan FUKI untuk menjadi motor geraknya dakwah. Kita bakal mengawal kegiatan mentoring di Fasilkom agar makin kece dari hari kehari, selain itu ada juga penjagaan ruhiyah anggota FUKI dan upgrading untuk pengurus FUKI dengan kajian islami yang seru, nggak ngebosenin, dan pastinya bermanfaat!
Yup, dari Pembinaanlah kekuatan-kekuatan dakwah Islam dibangun. Sudah siap menjadi bagian dari kebaikan?
					</p>
					<button class="md-close"><strong>Close!</strong></button>
				</div>
			</div>
		</div>
		<div class="md-modal md-effect-10" id="modal-Sospol">
			<div class="md-content">
				<h3>Sospol</h3>
				<div>
					<p>Di Sospol kamu bakal banyak jalanin proker yang seru dan bikin orang lain bahagia.
Mau tau kan keseruannya?? Yuk gabung di Sospol FUKI 2017.&#34;Bagi manusia ada malaikat-malaikat yang selalu mengikutinya bergiliran, dimuka dan dibelakangnya, mereka menjaganya atas perintah Allah, sesungguhnya Allah tidak merubah keadaan sesuatu kaum sehingga mereka merubah keadaan yang ada pada diri mereka sendiri dan apabila Allah menghendaki keburukan terhadap sesuatu kaum, maka tak ada yang dapat menolaknya, dan sekali-kali tak ada pelindung bagi mereka selain Allah.&#34; (QS Al-Ra&#39;d [13], 11)
<br>Mari bergerak bersama ciptakan suatu perubahan bersama FUKI 2017!
</p>
					<button class="md-close"><strong>Close!</strong></button>
				</div>
			</div>
		</div>
		<div class="md-modal md-effect-10" id="modal-paf">
			<div class="md-content">
				<h3>PAF</h3>
				<div>
					<p>PAF (Pengembangan Aset FUKI) adalah biro yang mengurus internal FUKI supaya menjadi keluarga yang kuat dan tahan lama(?) Mau bikin keluarga baru? Jangan salah, bukan sekadar keluarga biasa yang bakal kamu dapetin kalo gabung PAF. Kalo kamu senang berinteraksi dengan orang&#45;orang, punya ide � ide acara yang menarik, senang membantu dan memotivasi orang lain. Ngga salah lagi!! PAF adalah tempat yang cocok untuk kamu untuk mengembangkan potensi kamu dan juga tempat belajar yang asik karena bisa berhubungan dengan orang banyak!! Proker&#45;proker di PAF dijamin seru&#45;seru pokonya. Ga percaya? Buruan gabung!!
					<br>&#34;Orang mu&#39;min dengan mu&#39;min lainnya bagaikan suatu bangunan kokoh yang saling menguatkan antara satu dengan lainnya.&#34; (HR Bukhori, Muslim, Tarmidzi dan Nasai&#39; dari Abu Musa Al-Asy&#39;ari).
					<br>Di tunggu konstribusi kamu untuk menjadikan FUKI 2017 keluarga yang luar biasa ^^.
					</p><br>
					<button class="md-close"><strong>Close!</strong></button>
				</div>
			</div>
		</div>
		<div class="md-modal md-effect-10" id="modal-muc">
			<div class="md-content">
				<h3>MuC</h3>
				<div>
					<p>MuC adalah sebuah bidang yang istimewa. Apabila diibaratkan FUKI sebagai sebuah rumah, maka Bidang
Kemuslimahan merupakan suatu ruangan khusus untuk mempercantik para muslimah dari luar
maupun dalam dengan pengetahuan, keterampilan, serta core competence yang sesuai dengan
syariat Islam.</p>
					<button class="md-close"><strong>Close!</strong></button>
				</div>
			</div>
		</div>
		<div class="md-modal md-effect-10" id="modal-KMF">
			<div class="md-content">
				<h3>KMF</h3>
				<div>
					<p>Kalau kamu suka menulis, senang mencari tahu penulisan yang sesuai dengan EYD, mencintai keindahan dan kebersihan, juga ingin berkontribusi demi Musholla Al-Khawarizmi yang lebih nyaman, tandanya biro yang satu ini &#34;kamu banget&#34; deh.
Dimana lagi kalo bukan di KMF FUKI 2017! Yeay!
Mau gabung tapi merasa kurang mumpuni dalam hal kesekretariatan? Tenang, tak ada kata terlambat untuk belajar, kita bakal belajar bareng disini, kok.
Eitss, buat kamu yang berasal dari kau adam, kesempatan ini juga terbuka lebar untuk kamu loh!
Jadi, ingin mengangkasa bersama Biro KMF FUKI 2017? Buruan daftar di KMF Dan bersiaplah karena hal-hal seru akan menghampirimu
					</p>
					<button class="md-close"><strong>Close!</strong></button>
				</div>
			</div>
		</div>
		<div class="md-modal md-effect-10" id="modal-media">
			<div class="md-content">
				<h3>Media</h3>
				<div>
					<p> Media FUKI juga memproduksi majalah inspirasi, Nol Derajat! Sarana publikasi di Media FUKI tidak hanya berupa gambar (poster, spanduk) tetapi juga berupa Video Serial, Company Profile, Kenangan Akhir Tahun, dll. Masih banyak hal-hal menarik dan bermanfaat yang akan didapat pada Media FUKI. Waahhhh.. pasti semakin penasaran dengan Biro Media FUKI kan? Jangan hanya penasaran, yuk langsung ikut bergabung bersama kami di Media FUKI 2016. 
&#34;Barangsiapa yang melihat kemungkaran, maka cegahlah dengan tanganmu, apabila belum bisa, maka cegahlah dengan mulutmu, apabila belum bisa, cegahlah dengan hatimu, dan mencegah kemungkaran dengan hati adalah pertanda selemah-lemah iman&#34; &#45; (HR Muslim) 
Daftarkan dirimu sekarang juga dan mari berdakwah dengan menarik, kreatif, dan tentunya sesuai dengan 3 pedoman utama agama Islam.
					</p>
					<button class="md-close"><strong>Close!</strong></button>
				</div>
			</div>
		</div>
		<div class="md-modal md-effect-10" id="modal-Alquran">
			<div class="md-content">
				<h3>Alquran</h3>
				<div>
					<p>Di departemen ini kamu dapat menambah kemampuan kamu dalam membaca Al&#45;Qur&#39;an dan juga menambah pemahaman kamu terhadap kandungan yang ada di dalamnya. Bukan hanya dapat menambah kemampuan kamu loh, kamu juga dapat menebarkan manfaat Al&#45;Qur&#39;an ini kepada teman-temanmu, baik secara langsung maupun tidak.
Pernah denger hadits ini? &#34;Sebaik-baik kalian adalah yang mempelajari Al&#45;Qur&#39;an dan mengajarkannya.&#34; (HR Bukhari).
Yuk buat kamu yang ingin menjadi sebaik-baiknya manusia, langsung aja daftarkan dirimu untuk menjadi bagian dari Keluarga FUKI 2017!
					</p>
					<button class="md-close"><strong>Close!</strong></button>
				</div>
			</div>
		</div>
<div class="md-modal md-effect-10" id="modal-humas">
			<div class="md-content">
				<h3>Humas</h3>
				<div>
					<p>Humas bertanggung jawab untuk menghubungkan FUKI dan pihak luar FUKI.
Humas adalah biro yang bakal ngurus publikasi dan informasi yang keluar FUKI (aka Jarkoman) dan juga branding konten buat official accounts FUKI itu sendiri, biar FUKI lebih terkenal dan lebih kece, hehe.
Selain itu, Humas juga punya proker seperti Grand Launching buat mengenalkan FUKI dengan Fasilkom dan Tour de LDF, yaitu kunjungan FUKI ke LD lain! Seru banget ga sih?!! KUY lah daftar Humas! Kita sampaikan kabar gembira ke seluruh Fasilkomers!!					</p><br>
					<button class="md-close"><strong>Close!</strong></button>
				</div>
			</div>
		</div>
		<div class="md-modal md-effect-10" id="modal-fukicorp">
			<div class="md-content">
				<h3>FUKI Corp</h3>
				<div>
					<p>Dalam satu tahun yang indah ini kegiatan FUKICORP akan diisi dengan mencari dana untuk kegiatan dakwah FUKI. Tentu saja dengan cara-cara halal yang diridhoi Allah.
<br>&#34;Apabila telah ditunaikan shalat, maka bertebaranlah kamu di muka bumi (untuk mencari rezki dan usaha yang halal) dan carilah karunia Allah, dan ingatlah Allah banyak-banyak supaya kamu beruntung&#34; (QS al-Jumu&#39;ah:10)
<br>Karena di bumi ini dipenuhi rezeki dari Allah:)
<br>Ditunggu yaa kontribusimu :)</p>
					<br>
					<button class="md-close"><strong>Close!</strong></button>
				</div>
			</div>
		</div>
		<div class="md-modal md-effect-10" id="modal-itf">
			<div class="md-content">
				<h3>IT Force</h3>
				<div>
					<p>Beberapa tahun belakangan ini, alhamdulillah ITF (dulu Ukhuwah) telah membuat beberapa sistem informasi yang membantu menunjang program-program yang ada di FUKI.
Program kerja ITF saat ini ingin mewujudkan sistem yang lebih baik untuk FUKI! Buat yang suka UI/UX, ITF tahun ini juga akan mengimplementasi ilmu UI/UX dalam mengembangkan sistem! Intinya, ITF bisa jadi sarana belajar web Fasilkomers. ;)
Ayo gabung ITF dan bersama kita membuat aplikasi dan sistem informasi yang tentunya lebih inovatif untuk syiar FUKI yang lebih baik lagi!
Jadi tunggu apa lagi???
Kalau kamu mahasiswa Fasilkom yang tertarik dengan teknologi, ingin belajar ngoding atau malah udah jago ngoding, ITF akan mewadahi kamu! Kapan lagi kamu bisa mengaplikasikan ilmu yang kamu dapet di Fasilkom untuk berdakwah? Jadilah pakar IT Islami yang bisa berdakwah lewat ITF :) Eits, tapi jangan salah! Ngga harus jago ngoding buat gabung di ITF, yang penting niat yang tulus dan istiqomah untuk bekerja sama dalam kebaikan.					</p>
<br>
<button class="md-close"><strong>Close!</strong></button>
				</div>
			</div>
		</div>
		<div class="md-modal md-effect-10" id="modal-ki">
			<div class="md-content">
				<h3>Kontrol Internal</h3>
				<div>
					<p>Apa itu kontrol internal(KI)? Intinya, KI adalah biro yg bertugas menjaga mutu FUKI! Gimana cara ngejaganya? Kami mengadakan fit and proper test, evaluasi rutin, turun lapangan, dan memberikan masukan-masukan ke departemen/biro lain!
<br>Intinya, KI bekerja menjamin supaya semua proker FUKI tetap berjalan sesuai rencana, keren, dan bermanfaat buat Fasilkom :3
					</p>
					<button class="md-close"><strong>Close!</strong></button>
				</div>
			</div>
		</div>


	<!-- === MAIN Background === -->
	<div class="slide story" id="slide-1" data-slide="1">
		<div class="container home-row">
			<!-- <div id="home-row-1" class="row clearfix"> -->
				<div class="col-12">
					<h1 class="font-thin" id="welcome">PEREKRUTAN TERBUKA<br><br><span class="font-semibold" style="display:block; margin-top: 15px;"><img src="<?php echo base_url().'assets/img/logo fuki white.png';?>"> FUKI 2017<br></span></h1>
					<h3 class="font-semibold"><br>Impactful, Meaningful</h3>
					<p class="col-lg-8 col-sm-offset-2 font-thin">
					 FUKI 2017 memiliki semangat untuk menjadi lembaga dakwah yang senantiasa memberikan pengaruh positif terhadap masyarakat Fasilkom UI serta dirasakan keberadaannya dengan berorientasi pada Al-Qur'an dan As-Sunnah.
					</p>
				</div><!-- /col-12 -->
			<!-- </div> /row -->
			<div id="home-row-2" class="row clearfix">
				<div class="col-12 col-sm-12">
				<div class="home-hover" href="<?php echo base_url().'index.php/join/enter';?>" style="width: 200px; margin: 0 auto;"><a href="<?php echo base_url().'index.php/join/enter';?>" class="btn btn-primary btn-lg font-semibold" style="background-color:#ff9707">Daftar Sekarang</a></div></div>
			</div><!-- /row -->
		</div><!-- /container -->

<!-- === Tambahan === -->
	<div class="row slide-1-2" id="slide-1-2" data-slide="1-2"">
	<div class="container">
			<div class="row title-row">
				<div class="col-12 font-semibold">Dewan Pertimbangan FUKI</div>
			</div><!-- /row -->
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div><!-- /row -->
			<div class="row subtitle-row">
				<p></p>
				<div class="col-12 font-thin"><span class="font-semibold">Ketua</span><br>Fadhlan Hidayat, IK 2014</div>
			<!-- /row -->
			
				<div class="col-12 col-md-6 col-sm-6">
					<p>&nbsp;</p>
					<h3 class="font-thin"><span class="font-semibold">Syiar</span><br>Idris Izzaturrahman, SI 2014</h2>
					<h5 class="font-thin">&nbsp;</h5>
				</div><!-- /col12 -->
				<div class="col-12 col-md-6 col-sm-6">
					<p>&nbsp;</p>
					<h3 class="font-thin"><span class="font-semibold">Tarbawi</span><br>Sumayyah, SI 2014</h2>
					<h5 class="font-thin">&nbsp;</h5>
				</div><!-- /col12 -->	
			</div>
			<div class="row title-row">
				<div class="col-12 font-semibold">Ketua FUKI 2017</div>
			</div><!-- /row -->
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div><!-- /row -->
			<div class="row subtitle-row">
				<p></p>
				<div class="col-12 font-thin">Idris Izzaturrahman, SI 2014</div>
			</div><!-- /row -->
	</div><!-- /container -->
	</div><!-- /row -->
</div><!-- /slide1 -->
	

	<!-- === Slide 2 === -->
	<div class="slide story" id="slide-2" data-slide="2">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-thin"><span class="font-semibold">ALIANSI NONBIDANG</span></div>
			</div><!-- /row -->
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div><!-- /row -->
			<div class="row content-row">

				<div class="col-12 col-lg-6 col-sm-6">
					<p><img src="<?php echo base_url().'assets/img/bidang/big/bw/'.$img['Kontrol Internal'].'.png';?>"></p>
					<h2 class="font-thin"><span class="font-semibold">Kontrol Internal</span><br>Ahmad Nazhif Rahmatu, SI 2014</h2>
					<h4 class="font-thin">Apa itu Kontrol Internal (KI)? KI bertugas untuk menjaga mutu FUKI dengan berusaha menjamin semua proker FUKI berjalan sesuai rencana, keren, dan bermanfaat buat Fasilkom</h4>
					<br>Mau jadi KI FUKI? Daftar lagi tahun depan ya :)
				<!-- <button class="md-trigger" data-modal="modal-ki">Lebih lanjut</button> -->
				</div><!-- /col12 -->

				<div class="col-12 col-lg-6 col-sm-6">
					<p><img src="<?php echo base_url().'assets/img/bidang/big/bw/'.$img['MuC'].'.png';?>"></p>
					<h2 class="font-thin"><span class="font-semibold">Muslimah Center</span><br>Sarah Yarismal, SI 2014</h2>
					<h4 class="font-thin">MuC menjadi ruangan khusus untuk para muslimah berkreasi dan berbagi, belajar, serta menginspirasi melalui rangkaian kegiatan kemuslimahan.</h4>
					<br><button class="md-trigger" data-modal="modal-muc">Lebih lanjut</button>

				</div><!-- /col12 -->

			</div><!-- /row -->
		</div><!-- /container -->
	</div><!-- /slide2 -->

	<div class="slide story" id="slide-3" data-slide="3">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-thin"><span class="font-semibold">KEUANGAN</span></div>
			</div><!-- /row -->
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div><!-- /row -->
			<div class="row subtitle-row">
				<div class="col-12 col-lg-6 col-sm-6">
					<p><img src="<?php echo base_url().'assets/img/bidang/big/bw/'.$img['Controller'].'.png';?>"></p>
					<h2 class="font-thin"><span class="font-semibold">Controller</span><br>Intan Dwi Hapsari, SI 2014</h2>
					<h4 class="font-thin">Mengatur dana yang masuk dari fakultas serta berfungsi sebagai supervisor bidang FUKI Corp</h4>
				</div><!-- /col12 -->
				<div class="col-12 col-lg-6 col-sm-6">
					<p><img src="<?php echo base_url().'assets/img/bidang/big/bw/'.$img['Bendahara'].'.png';?>"></p>
					<h2 class="font-thin"><span class="font-semibold">Treasurer</span><br>Fatah Fadhlurrohman, IK 2014</h2><!-- !!! -->
					<h4 class="font-thin">Mengelola dana internal fuki dan melakukan komunikasi dengan bendahara bidang</h4>
				</div><!-- /col12 -->

			</div><!-- /row -->
			<div class="row content-row">
				<div class="col-12 col-lg-6 col-sm-6 col-sm-offset-3">
					<p><img src="<?php echo base_url().'assets/img/bidang/big/bw/'.$img['FUKI Corp'].'.png';?>"></p>
					<h2 class="font-semibold">FUKI Corp<br><span class="font-thin">Ahmad Elang Islam B., IK 2015</span></h2>
					<h4 class="font-thin">Ingin belajar mencari uang? Merasa punya ide bisnis yang menarik? Ingin belajar proyekan?  Ingin berjualan di bazaar beruang? Atau terlalu banyak waktu luang? Tenang saja, kami punya solusinya: ikut FUKI Corp!</h4>
					<br><button class="md-trigger" data-modal="modal-fukicorp">Lebih lanjut</button>
				</div><!-- /col12 -->

			</div><!-- /row -->
		</div><!-- /container -->
	</div><!-- /slide3 -->

	
	<!-- === Slide 4 - INTERNAL === -->
	<div class="slide story" id="slide-4" data-slide="4">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-thin"><span class="font-semibold">INTERNAL</span></div>
			</div><!-- /row -->
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div><!-- /row -->
			<div class="row subtitle-row">
				<div class="col-12 font-thin"><span class="font-semibold">Koordinator Bidang Internal</span><br>Ayu Fatmawati, SI 2014</div>
			</div><!-- /row -->
			<div class="row content-row">
				<div class="col-12 col-lg-4 col-sm-6">
					<p><img src="<?php echo base_url().'assets/img/bidang/big/bw/'.$img['KMF'].'.png';?>"></p>
					<h2 class="font-thin"><span class="font-semibold">KMF</span><br>Hanifa Arrumaisha, SI 2016</h2>
					<h4 class="font-thin">&#34;Sesungguhnya Allah itu indah, dan menyukai keindahan.&#34; (H.R. Muslim)

Cari tahu lebih dalam tentang Biro KMF, Yuk!</h4>
					<br><button class="md-trigger" data-modal="modal-KMF">Lebih lanjut</button>
				</div><!-- /col12 -->
				<div class="col-12 col-lg-4 col-sm-6">
					<p><img src="<?php echo base_url().'assets/img/bidang/big/bw/'.$img['PAF'].'.png';?>"></p>
					<h2 class="font-thin"><span class="font-semibold">PAF</span><br>Eldy Hidayat, IK 2015</h2>
					<h4 class="font-thin">PAF apa sih? Kok baru denger ya? PAF adalah Pengembangan Aset FUKI, keren kan namanya?!</h4>
					<br><button class="md-trigger" data-modal="modal-paf">Lebih lanjut</button>
				</div><!-- /col12 -->
				<div class="col-12 col-lg-4 col-sm-6">
					<p><img src="<?php echo base_url().'assets/img/bidang/big/bw/'.$img['ITF'].'.png';?>"></p>
					<h2 class="font-thin"><span class="font-semibold">ITF</span><br>Luthfi Abdurrahman, SI 2014</h2>
					<h4 class="font-thin">Sudah tahu siapa yang ada di balik website pendaftaran FUKI ini?
FUKI punya biro IT sendiri loh yang mengurusi segala per-IT-an ini :D Penasaran apa itu ITF?
</h4>
					<br><button class="md-trigger" data-modal="modal-itf">Lebih lanjut</button>
				</div><!-- /col12 -->
			</div><!-- /row -->
		</div><!-- /container -->
	</div><!-- /slide4 -->

	<!-- === SLide 5 - SYIAR -->
	<div class="slide story" id="slide-5" data-slide="5">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-thin"><span class="font-semibold">SYIAR</span></div>
			</div><!-- /row -->
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div><!-- /row -->
			<div class="row subtitle-row">
					<div class="col-12 font-thin"><span class="font-semibold">Koordinator Bidang Syiar</span><br>Sumayyah, SI 2014</div>
			</div><!-- /row -->
			<div class="row content-row">

				<div class="col-12 col-lg-3 col-sm-3">
					<p><img src="<?php echo base_url().'assets/img/bidang/big/bw/'.$img['Sospol'].'.png';?>"></p>
					<h2 class="font-thin"><span class="font-semibold">Sospol</span><br>Ismail Al Ghani, SI 2016</h2>
					<h4 class="font-thin">Kamu tertarik dengan isu sosial politik Islam? Ingin membantu dan jadi bermanfaat? Realisasikan itu semua di dalam Sospol FUKI 2017.
Merasa minder karena belum berpengalaman? Jangan takut, kita bakal belajar bareng disini kok :)</h4>
					<br><button class="md-trigger" data-modal="modal-Sospol">Lebih lanjut</button>
				</div><!-- /col12 -->
				<div class="col-12 col-lg-3 col-sm-3">
					<p><img src="<?php echo base_url().'assets/img/bidang/big/bw/'.$img['Syiar'].'.png';?>"></p>
					<h2 class="font-thin"><span class="font-semibold">Syiar</span><br>Andri Nur Rochman, IK 2015</h2>
					<h4 class="font-thin">Syiar apa sih, kok berat banget bahasanya?
Eiittzz.. Jangan salah broo! Syiar menurut bahasa artinya rasa loh! Gak serem kan? Syiar juga bisa diartikan menyampaikan kabar kepada orang yang tidak tahu agar menjadi tahu. Tsadeest!
</h4>				
		
					<br><button class="md-trigger" data-modal="modal-syiar">Lebih lanjut</button>
				</div><!-- /col12 -->
				<div class="col-12 col-lg-3 col-sm-3">
					<p><img src="<?php echo base_url().'assets/img/bidang/big/bw/'.$img['Alquran'].'.png';?>"></p>
					<h2 class="font-thin"><span class="font-semibold">Alquran</span><br>M. Farhan Mardadi, SI 2015</h2>
					<h4 class="font-thin">Ingin lebih dekat dan berbagi kebaikan dengan Al&#45;Qur&#39;an? Wah kamu orang yang kami cari untuk menjadi bagian Departemen Al&#45;Qur&#39;an FUKI 2017. Di departemen ini, kita bisa belajar lebih banyak tentang Al&#45;Qur&#39;an </h4>
					<br><button class="md-trigger" data-modal="modal-Alquran">Lebih lanjut</button>
				</div><!-- /col12 -->
				<div class="col-12 col-lg-3 col-sm-3">
					<p><img src="<?php echo base_url().'assets/img/bidang/big/bw/'.$img['Pembinaan'].'.png';?>"></p>
					<h2 class="font-thin"><span class="font-semibold">Pembinaan</span><br>Adzkia Husnul Abidat, SI 2015</h2>
					<h4 class="font-thin">Tahukah kamu jauuuh sebelum ada FUKI, Pembinaan sudah ada? Bahkan, sejak puluhan bahkan ratusan tahun lalu, Pembinaan sudah ada lhoo! Hah? Kok bisa? Iya! Pembinaan sudah ada sejak jaman Rasulullah :D</h4>
					<br><button class="md-trigger" data-modal="modal-pembinaan">Lebih lanjut</button>
				</div><!-- /col12 -->
			</div><!-- /row -->
		</div><!-- /container -->
	</div><!-- /slide5 -->
	
	<!-- === Slide 6 - RELASI === -->
	<div class="slide story" id="slide-6" data-slide="6">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-thin"><span class="font-semibold">RELASI</span></div>
			</div><!-- /row -->
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div><!-- /row -->
			<div class="row subtitle-row">
				<div class="col-12 font-thin"><span class="font-semibold">Koordinator Bidang Relasi</span><br>Irma Latifatul Laily, IK 2014</div>
			</div><!-- /row -->
			<div class="row content-row">
				<div class="col-12 col-lg-6 col-sm-6">
					<p><img src="<?php echo base_url().'assets/img/bidang/big/bw/'.$img['Humas'].'.png';?>"></p>
					<h2 class="font-thin"><span class="font-semibold">Humas</span><br>Farhan N. P., SI 2015 </h2>
					<h4 class="font-thin">&#34;Dan sampaikanlah berita gembira kepada mereka yang beriman dan berbuat baik&#34; (QS 2 : 25).
Apatuh Biro Humass??? Kalo, FUKI itu perusahaan misalnya, berarti humas itu seperti public relation. Tugas Humas simple kok!</h4>
					<br><button class="md-trigger" data-modal="modal-humas">Lebih lanjut</button>
				</div><!-- /col12 -->
				<div class="col-12 col-lg-6 col-sm-6">
					<p><img src="<?php echo base_url().'assets/img/bidang/big/bw/'.$img['Media'].'.png';?>"></p>
					<h2 class="font-thin"><span class="font-semibold">Media</span><br>Muhammad Akhdan Shidqi, SI 2015</h2>
					<h4 class="font-thin">Pernah liat gambar beserta quotes nasihat berisikan ayat Al-qur�an atau hadist yang lebih sering disebut &#34;Selembar Nasihat&#34;? Poster-poster kegiatan FUKI? Nah, itu semua hasil karya biro Media FUKI loh! </h4>
					<br><button class="md-trigger" data-modal="modal-media">Lebih lanjut</button>					
				</div><!-- /col12 -->
			</div><!-- /row -->
		</div><!-- /container -->
	</div><!-- /slide6 -->

	<!-- === CONTACT === -->
	<div class="slide story" id="slide-7" data-slide="7">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-semibold">Ayo kencangkan sabuk pengamanmu! <h3 class="font-light"><br>FUKI 2017 siap mengajakmu untuk #BersamaMengangkasa! Bagaimana denganmu?</h3></div>
			</div><!-- /row -->
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div><!-- /row -->
			<div class="row subtitle-row">
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
				<div class="col-12 col-sm-10 font-light"></div>
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
			</div><!-- /row -->
			<div class="row">
			<div class="col-12 col-sm-12">
				<div class="home-hover" href="<?php echo base_url().'index.php/join/enter';?>" style="width: 200px; margin: 0 auto;"><a href="<?php echo base_url().'index.php/join/enter';?>" class="btn btn-primary btn-lg" style="width: 200px;"><h2 class="font-semibold" style="margin-top: 0px">Aku Siap!</h2></a></div></div>
			</div>
			<div id="contact-row-4" class="row">
				<div class="col-12 col-sm-3 with-hover-text">
					<p><a target="_blank" href="#"><i class="icon icon-phone"></i></a></p>
					<span class="hover-text font-light ">+62 821-4032-6563</span></a>
				</div><!-- /col12 -->
				<div class="col-12 col-sm-3 with-hover-text">
					<p><a target="_blank" href="#"><i class="icon icon-envelope"></i></a></p>
					<span class="hover-text font-light ">fukifasilkomui@gmail.com</span></a>
				</div><!-- /col12 -->
				<div class="col-12 col-sm-3 with-hover-text">
					<p><a target="_blank" href="https://facebook.com/fuki.fasilkom"><i class="icon icon-user"></i></a></p>
					<span class="hover-text font-light ">fuki.fasilkom</span></a>
				</div><!-- /col12 -->
				<div class="col-12 col-sm-3 with-hover-text">
					<p><a target="_blank" href="https://twitter.com/fukifasilkom"><i class="icon icon-comment"></i></a></p>
					<span class="hover-text font-light ">@fukifasilkom</span></a>
				</div><!-- /col12 -->
			</div><!-- /row -->
		</div><!-- /container -->
	</div><!-- /Slide 8 -->
  
         <div class="md-overlay"></div><!-- the overlay element -->

		<!-- classie.js by @desandro: https://github.com/desandro/classie -->
		<script src="<?php echo base_url().'assets/front/';?>js/classie.js"></script>
		<script src="<?php echo base_url().'assets/front/';?>js/modalEffects.js"></script>

	
</body>

	<!-- SCRIPTS -->
      
	<script src="<?php echo base_url().'assets/front/';?>js/html5shiv.js"></script>
	<script src="<?php echo base_url().'assets/front/';?>js/jquery-1.10.2.min.js"></script>
	<script src="<?php echo base_url().'assets/front/';?>js/jquery-migrate-1.2.1.min.js"></script>
	<script src="<?php echo base_url().'assets/front/';?>js/bootstrap.min.js"></script>
	<script src="<?php echo base_url().'assets/front/';?>js/jquery.easing.1.3.js"></script>
	<script src="<?php echo base_url().'assets/front/';?>js/script.js"></script>
</html>
