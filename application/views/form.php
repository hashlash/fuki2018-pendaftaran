<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>FUKI 2017</title>
        <meta name="viewport" content="width=device-width">
        
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.ico" type="image/x-icon">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
        <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
        
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,300,200&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    
        <?php
        $image_bidang = get_image_bidang();
        $bidang = get_bidang();
        ?>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <div style="background: #4F5D73; height: 10px; width: 100%"></div>
        <div id="wrapper">
        
        <div class="row">
            <a href="<?php echo base_url();?>" style="color: #333333; text-decoration: none">
            <div class="col-lg-1 col-md-1 col-sm-1" style="padding-right: 0">
                <img src="<?php echo base_url().'assets/img/logo fuki black.png';?>" style="margin-top: 27px">
            </div>
  
            <div class="col-lg-11 col-md-11 col-sm-11" style="padding-left: 0">
            <h1 style="margin-bottom: 2px;">Perekrutan Terbuka FUKI 2017</h1>
            <h3 style="margin-top: 0">#MengangkasaBersama</h3>
            </div>
            </a>
        </div>
        <br>
      
        <div class="row">

<span style="display:none;">
        <div class="col-lg-7 col-md-7">
        <h2>Daftar</h2>
        <small>*Pendaftar harus merupakan mahasiswa/i Fasilkom UI yang beragama Islam</small><br>
        <?php 
        if ( isset($register_error) ) print_error('Register gagal', $register_error);
        if ( isset($register_success) ) print_success('Register berhasil, segera login dengan email dan password yang baru didaftarkan untuk melihat tugas.', $register_success);
        // if ( isset($post) )debug($post);
        ?>
        <form method="post">
            <div class="row">
                <div class="form-group col-lg-6 col-md-6">
                <h4>Nama</h4><input type="text" class="form-control" placeholder="Nama Lengkap" name="nama" required>
                </div> <!-- /col -->
                <div class="form-group col-lg-6 col-md-6">
                <h4>NPM</h4><input type="text" class="form-control" placeholder="NPM" name="npm" required>
                </div>
            </div> <!-- /row -->
            
            <div class="row">
                <div class="form-group col-lg-6 col-md-6">
                <h4>Jenis Kelamin</h4>
                <div class="radio"><label><input type="radio" name="gender" value="L" checked required>Laki-laki</label></div>
                <div class="radio"><label><input type="radio" name="gender" value="P">Perempuan</label></div>
                </div>
                
                <div class="form-group col-lg-6 col-md-6">
                <h4>Jurusan</h4>
                <div class="radio"><label><input type="radio" name="jurusan" value="ik" checked required>Ilmu Komputer</label></div>
                <div class="radio"><label><input type="radio" name="jurusan" value="si">Sistem Informasi</label></div>
                </div>
            </div> <!-- /row -->
            
            <div class="row">
                <div class="form-group col-lg-6 col-md-6">
                <h4>Email</h4><input type="email" placeholder="Email" class="form-control" name="email" required>
                </div>
            </div> <!-- /row -->
            
                
            <div class="row">
                <div class="form-group col-lg-6 col-md-6">
                <h4>Nomor HP</h4><input type="text" placeholder="+628xxxxxxx" class="form-control" name="no_hp" required>
                </div>
            </div> <!-- /row -->

            <div class="row">
                <div class="form-group col-lg-6 col-md-6">
                <h4>Password</h4><input type="password" placeholder="Password" class="form-control" name="password" required>
                </div>
            </div> <!-- /row -->
            
            <div class="form-group">
            <div id="pil_bidang_1">
                <h4>Pilihan bidang 1</h4>
                <input type="hidden" name="pil_bidang_1"><br>
                <?php
                $counter = 0;
                for ( $i = 0; $i < get_number_pil_bidang(); $i++ ) {
                    if ( $counter % 6 == 0 ) echo '<div class="row">';
                    if ( $counter == 6 ) echo '<div class="col-lg-1 col-md-1"></div>';
                    echo "<div class='box-bidang col-lg-2 col-md-4 col-sm-6'>
                    <img class='bw' src='".base_url()."assets/img/bidang/bw/$image_bidang[$i].png'>
                    <img class='fc fuki-hide' src='".base_url()."assets/img/bidang/$image_bidang[$i].png'>
                    <h4>$bidang[$i]</h4></div> ";
                    
                    if ( $counter == 10 ) echo '<div class="col-lg-1 col-md-1"></div>';
                    if ( $counter % 6 == 5 ) echo '</div>';
                    $counter++;
                }
                ?>
                </div>
                <div class="awas"></div>
            </div>
            </div>
            
            <div class="form-group">
            <div id="pil_bidang_2">
                <h4>Pilihan bidang 2</h4>
		<span>Jika hanya memilih 1 bidang, tidak perlu memilih pilihan bidang 2</span>
                <input type="hidden" name="pil_bidang_2" value="none"><br>
                <?php
                $counter = 0;
                for ( $i = 0; $i < get_number_pil_bidang(); $i++ ) {
                    if ( $counter % 6 == 0 ) echo '<div class="row">';
                    if ( $counter == 6 ) echo '<div class="col-lg-1 col-md-1"></div>';
                    echo "<div class='box-bidang col-lg-2 col-md-4 col-sm-6'>
                    <img class='bw' src='".base_url()."assets/img/bidang/bw/$image_bidang[$i].png'>
                    <img class='fc fuki-hide' src='".base_url()."assets/img/bidang/$image_bidang[$i].png'>
                    <h4>$bidang[$i]</h4></div> ";
                    
                    if ( $counter == 10 ) echo '<div class="col-lg-1 col-md-1"></div>';
                    if ( $counter % 6 == 5 ) echo '</div>';
                    $counter++;
                }
                ?>
                </div>
                <div class="awas"></div>
            </div>
            </div><br>
            <input type="submit" id="daftar" class="btn btn-info btn-lg" name="daftar" value="Daftar" style="width:100%;">
        </form>
        </div>
</span>        
        <div class="col-lg-1 col-md-1"></div>
        
        <div class="col-lg-4 col-md-4">
        <h2>Masuk</h2>
        <?php if ( isset($login_error) ) print_error('Login gagal', $login_error);?>
        <small>Sudah daftar? Silahkan masuk</small>    
        <form method="post">
            <div class="form-group">
            <h4>Email</h4><input type="email" class="form-control" name="email" required placeholder="Masukkan email"></label>
            </div>
            <div class="form-group">
            <h4>Password</h4><input type="password" class="form-control" name="password" required placeholder="Masukkan Password">
            </div><br>
            <input type="submit" class="btn btn-info btn-lg" name="masuk" value="Masuk" style="width:100%;">
        </form>
        </div> <!-- / .col -->
        </div> <!-- / .row -->
        </div> <!-- / #wrapper -->
        <div style="background: #4F5D73; height: 15px; width: 100%"></div>
        
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.9.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        <script>
            $(document).ready(function() {   
                $('.fc').hide().removeClass('fuki-hide');
                function setting_img(id) {  
                    $('#'+id+' .box-bidang').on('click', function() {
                        if ( $(this).hasClass('selected') ) {
                            // turn off
                            $(this).find('img.fc').hide();
                            $(this).find('img.bw').show();
                            
                            $(this).removeClass('selected');
                            $('#'+id+' input').val('');
                        }
                        else {
                            // turn on
                            $('#'+id+' .box-bidang.selected').removeClass('selected');
                            $('#'+id+' .box-bidang .fc').hide();
                            $('#'+id+' .box-bidang .bw').show();
                            
                            $(this).find('img.bw').hide();
                            $(this).find('img.fc').show();
                            
                            $(this).addClass('selected');
                            $('#'+id+' input').val($(this).find('h4').html());
                            $('#'+id+' .awas').html(''); // empty alert
                        }
                    });
                }
                setting_img('pil_bidang_1');
                setting_img('pil_bidang_2');
                $('#daftar').on('click', function(e) {
                   if ( $('#pil_bidang_1 input').val() === "" ) {
                       e.preventDefault();
                       $('#pil_bidang_1 .awas').html('<div class="alert alert-danger">Pilihan bidang 1 harus dipilih.</div>');
                   } 
                   
                   if ( $('#pil_bidang_2 input').val() == $('#pil_bidang_1 input').val() ) 
                       $('#pil_bidang_2 input').val('none');
                });
            });
        </script>
    </body>
</html>
